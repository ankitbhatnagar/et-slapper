module gitlab.com/ankitbhatnagar/et-slapper

go 1.19

require (
	github.com/getsentry/sentry-go v0.16.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/wayneashleyberry/terminal-dimensions v1.1.0 // indirect
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec // indirect
	golang.org/x/text v0.3.7 // indirect
)
